instrument-gui
==============

Package for controlling various instruments we have in our lab, powered by
PyVISA and GTK.


dependencies
------------

I haven't tested in a totally clean environment but I think the dependencies of
this project are as follows:

On Archlinux:

```sh
sudo pacman -S python-matplotlib python-gobject
pip install pyvisa pyvisa-py
```

On Debian:

```sh
sudo apt install python3-gi python3-pyvisa python3-pyvisa-py
sudo apt install gir1.2-gtk-4.0 libatlas-base-dev
pip3 install -U matplotlib
```

installation
------------

Remember also to give your user permissions to access any instruments, by adding
the following to /etc/udev/rules.d/99-com.rules:

```
SUBSYSTEM=="usb", MODE="0666", GROUP="usbusers"
```

then reload udev rules:

```sh
sudo udevadm control --reload-rules
sudo udevadm trigger
```

minimal standalone example
--------------------------

In case you don't want to use this GUI, which is probably a good decision for
advanced use cases, you can always write the script yourself. For example:

```py
#!/usr/bin/env python3
import pyvisa
import matplotlib.pyplot as plt
import time

rm = pyvisa.ResourceManager()
# if you want to get the resource IDs, use rm.list_resources()
dpo = rm.open_resource("USB0::1689::927::C030124::0::INSTR")

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

dpo.write("defaultsetup")
dpo.write("data:source ch1")
dpo.write("data:encdg ribinary")
dpo.write("data:resolution full")
dpo.write("data:composition singular_yt")
dpo.write("data:wfmoutpre:byt_nr 2")
dpo.write("data:start 1")
dpo.write("data:stop 100")

time.sleep(2)
curve = dpo.query_binary_values("curve?", datatype="h", is_big_endian=True)
x = range(len(curve))

ax.plot(x, curve, "-")
plt.show()
```

